from moviepy.editor import *
import glob
import os
import numpy as np
import traceback
import sys

#w = 1280
#h=800
w=1980
h=1080
moviesize = w,h
def f(t, size, a=np.pi / 3, thickness=20):
    w, h = size
    v = thickness * np.array([np.cos(a), np.sin(a)])[::-1]
    center = [int(t * w / duration), h / 2]
    return biGradientScreen(size, center, v, 0.6, 0.0)


shade = ColorClip(moviesize, color=(0, 0, 0))
mask_frame = lambda t: f(t, moviesize, duration)
#shade.mask = VideoClip(is_mask=True, get_frame=mask_frame)



def get_clip():
    
    result=[]   
    
    directory=os.getcwd()+"/img_cards/"
    img_cards_files = glob.glob(directory+'*.png')
    img_cards_files.sort(reverse=True)

    directory=os.getcwd()+"/img_value/"
    img_value_files = glob.glob(directory+'*.png')
    img_value_files.sort(reverse=True)

    
    clip_start=0
    duration=13

    for card,value in zip(img_cards_files,img_value_files):

        y_position=322

        ##Card IMG Cip
        clip = ImageClip(card).resize(1.20)
        clip_w,clip_h=clip.size
        clip=clip.set_position(lambda t: (w - (w-500)*(t/7)+60,y_position))
        clip=clip.set_start(clip_start).set_duration(duration)
    
        ##Popup IMG clip 
        popup_clip = ImageClip(value,transparent=True).resize(1.25)
        popup_clip=popup_clip.set_position(lambda t: (w - (w-500)*(t/7)+90,max(y_position-300, 500-pow(7,t)) ))
        popup_clip=popup_clip.set_start(clip_start).set_duration(duration)
        
        
        clip_start+=2
        result.append(popup_clip)
        result.append(clip)

    #result=concatenate_videoclips(result)

    return result

def get_clip_test():
    
    result=[]   
    
    directory=os.getcwd()+"/img_cards/"
    img_cards_files = glob.glob(directory+'*.png')[:1]
    img_cards_files.sort(reverse=True)

    directory=os.getcwd()+"/img_value/"
    img_value_files = glob.glob(directory+'*.png')[:1]
    img_value_files.sort(reverse=True)

    
    clip_start=0
    duration=1

    for card,value in zip(img_cards_files,img_value_files):

        y_position=250 

        ##Card IMG Cip
        clip = ImageClip(card)
        clip_w,clip_h=clip.size
        clip=clip.set_position('center',y_position)
        clip=clip.set_duration(duration)
    
        ##Popup IMG clip 
        popup_clip = ImageClip(value,transparent=True) 
        popup_clip=popup_clip.set_position('center',y_position)
        popup_clip=popup_clip.set_duration(duration)
        
        
        clip_start+=0.5
        result.append(popup_clip)
        result.append(clip)

    #result=concatenate_videoclips(result)

    return result

import random

if __name__ == '__main__':
    file_name='twitter follower'#sys.argv[1]

    result=get_clip()
    #result.write_videofile('final_test2.mp4'szfps=60,codec='nvenc')

    music_lib= os.getcwd()+'/audio'
    music_filelist=[x for x in glob.glob(os.path.join(music_lib, '*')) if '.md' not in x]  
    audioclip = AudioFileClip(random.choice(music_filelist))

    
     

    videoclip = CompositeVideoClip(result,size = moviesize)
    videoclip.audio=audioclip.audio_loop(duration=videoclip.duration).audio_fadeout(5.0)

    videoclip.to_videofile('video/{}.mp4'.format(file_name),fps=60,codec='libx264') 
#final.ipython_display(width = 480)
    
    

#hexagon="clip/hexagon36.png"
#hexagon_clip = ImageClip(hexagon) 
#hexagon_clip=hexagon_clip.set_position(lambda t: (800, max(100, 500-pow(300,t)) ))


