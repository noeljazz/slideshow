from moviepy.editor import *
import glob
import os
import random
w = 1980
#h = w*9/16 # 16/9 screen
h=1080
moviesize = w,h


def get_clip():
	 
	result=[]
	
	folders=["/img_cards/"]
	for folder in folders:
	    directory=os.getcwd()+folder
	    files = glob.glob(directory+'*.png')

	files.sort(reverse=True)
	
	clip_start=0
	y_position=100
	for file in files:
	    clip = ImageClip(file).resize(1.40) 
	    clip_w,clip_h=clip.size
	    clip=clip.set_position(lambda t: (w - (w-500)*(t/7)+60,y_position))
	  
	    clip=clip.set_start(clip_start).set_duration(13)
	    clip_start+=2.5

	    result.append(clip)
	'''

	file=files[-1]
	clip = ImageClip(file) 
	clip_w,clip_h=clip.size
	
	clip=clip.set_position(lambda t: (max((w - (1080-500)*(t/5),500)),'center'))


	clip=clip.add_mask().rotate(lambda t : min(90*t,360),expand=True).set_duration(7)
		

	#clip=clip.set_start(clip_start).set_duration(15)

	result.append(clip)
	'''
	return result


def generate_video_nopopup(file_name):
	music_lib= os.getcwd()+'/audio'
	music_filelist=[x for x in glob.glob(os.path.join(music_lib, '*')) if '.md' not in x]  
	audioclip = AudioFileClip(random.choice(music_filelist))

	videoclip = CompositeVideoClip(get_clip(),size = moviesize)
	videoclip.audio=audioclip.audio_loop(duration=videoclip.duration).audio_fadeout(5.0)
	#file_name='All Disney Movies by History'
	videoclip.to_videofile('video/{}.mp4'.format(file_name),fps=60,codec='libx264') 

	
#final.fps=60
#final.write_videofile('test.mp4', fps=60)


#final.ipython_display(width = 480)

if __name__ == '__main__':
	generate_video_nopopup('test')



 

    


