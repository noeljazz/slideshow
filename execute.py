from generate_cards_gspread import *
from video_templete_popup import *
from video_templete_nopopup import *
import traceback
sys.path.append(os.getcwd()+'/g_drive')
#from upload import *
import glob
from twx.botapi import TelegramBot, ReplyKeyboardMarkup

def message(text):
	bot = TelegramBot('1830407895:AAHrmXx-6SW_lPjVc5vaQSW7XxJTvrwekwY')
	updates = bot.get_updates().wait()
	print(updates)
	##Test
	
	bot.send_message(168658112, text).wait()
	#bot.send_message(-441070013, text).wait()



def remove():
    # print( os.getcwd())
    os.system('sudo chmod -R 777 .')
    p = os.path.abspath('')
    folders = ['/img_cards/','/img_value/']
    for folder in folders:
        directory = p + folder
        files = glob.glob(directory + '*')
        for f in files:
            if '.md' in f:
                pass
            else:
                os.remove(f)


##No popup value google doc templete:
##https://docs.google.com/spreadsheets/d/1dSo-3hct1YPhq7CEmZAIBL1LQHx6SPpXQM_1y-DmWcY/edit#gid=1624396226

##1.Load G sheet
sheet=Gsheets()
df=sheet.read()


##2. Check if df is none
if df is None :
	exit()

else:
	remove()


#3.generate image
try:
	
	generate_card(df=df)
	sheet.update(status_code=1)
except:
	error= (traceback.format_exc())
	print(error)
	sheet.update(status_code=error)
	exit()

 



#4.Generate VIdeo##
try:
	file_name=df.name


	##check if img_value exist
	img_value_filelist=[x for x in glob.glob(os.path.join(os.getcwd()+'/img_value', '*')) if '.md' not in x]  

	if len(img_value_filelist)==0:
		generate_video_nopopup(file_name=file_name)
	else:
		generate_video_popup(file_name=file_name)

	sheet.update(status_code=2)
	text='Done!\n' +file_name 
	message(text)
except:
	error= (traceback.format_exc())
	print(error)
	sheet.update(status_code=error)
	message('Error!\n'+file_name)
	exit()		





##update history tab
sheet.update_history_tab()
 