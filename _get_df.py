# -*- coding: UTF-8 -*-
from functools import reduce
import pandas as pd
import json
import numpy as np
from wiki_api import *
#https://www.educative.io/blog/pandas-cheat-sheet

def millify(n):
    import math
    millnames = ['',' K',' M',' B',' T']
    #millnames = ['']#,' Thousand']#,' M']
    n = float(n)
    millidx = max(0,min(len(millnames)-1,
                        int(math.floor(0 if n == 0 else math.log10(abs(n))/3))))

    return '{:,.2f}{}'.format(n / 10**(3 * millidx), millnames[millidx])

def df_get():
    ##MAKE SURE ONLY SELECT SELECTED DATA ONLY.CSV FULL INDICATOR HAS DUPLICATES
    file_name='db/salary.csv'
    
    df=pd.read_csv(file_name)    
    df['rank']=df['value'].rank(ascending=False).astype(int)
    df.sort_values(by='rank',ascending=False,inplace=True)
    return df 

def df_get():
    ##MAKE SURE ONLY SELECT SELECTED DATA ONLY.CSV FULL INDICATOR HAS DUPLICATES
    file_name='Best seeling video games.csv'
    pd.options.display.float_format = '{:,}'.format
    df=pd.read_csv(file_name)
    df['value']=df['value'].apply(lambda x: "{:,}".format(x,axis=1))

    #df["keyword"] = df["name"] +" "+ df["Year"].astype('str')    
    #df.sort_values(by='rank',ascending=False,inplace=True)
    #df['Image URL']=df['name'].apply(wiki_thumbnail_main_page)
    #df.to_csv('Best seeling video games.csv')
    return df 



def df_get():
    file_name='NTTT- Source - Sheet87.csv'
    df=pd.read_csv('db_parsed/'+file_name)
    #df['wikilink']=df['name'].apply(wiki_mainpage_url)
    df['image_url']=df['name'].apply(wiki_thumbnail_main_page)
    df.to_csv(file_name)
    return df
    def parse():
        ##MAKE SURE ONLY SELECT SELECTED DATA ONLY.CSV FULL INDICATOR HAS DUPLICATES
        file_name='db/NTTT- Source - MMO Game Total players.csv'
        df=pd.read_csv(file_name)
        df['value']=df['value'].astype(int)
        df['value']=df['value'].apply(lambda x: millify(x))
        
        df['Image URL']=df['name'].apply(wiki_thumbnail_main_page)
        df.to_csv('MMO Games - Total Player.csv')
      
        return df 



if __name__ == '__main__':
    
    df_get()
