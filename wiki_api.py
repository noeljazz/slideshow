import wikipedia
import requests
import json
#api url
#https://en.wikipedia.org/w/api.php?action=parse&prop=sections&page=Highest-paid_NBA_players_by_season&section=1&action=parse&section=3

#page = wikipedia.page(title="Stephen Curry")


from wikitables import import_tables
import pandas as pd


def wiki_thumbnail(keyword):
    headers = {
    "User-Agent": "Mozilla/5.0 (Windows NT 6.1; WOW64; rv:20.0) Gecko/20100101 Firefox/75.0",
    "Accept-Encoding": "gzip, deflate",
    "Accept": "text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8",
    "Accept-Language": "en-US,en;q=0.5",
    "Connection": "keep-alive"
    }
    print(keyword)
    url='https://en.wikipedia.org/w/api.php'
    payload = {'action': 'query', 'titles': keyword,'prop':'pageimages','format':'json','pithumbsize':200}
    r = requests.get(url, params=payload,headers=headers)
    data=json.loads(r.text)
    print(r.text)
    try:
        img=list(data['query']['pages'].values())[0]['thumbnail'].get('source','')
    except:
        img=''
    print (img)
    print('\n')
    return img


def wiki_thumbnail_main_page(keyword):
    try:
        headers = {
        "User-Agent": "Mozilla/5.0 (Windows NT 6.1; WOW64; rv:20.0) Gecko/20100101 Firefox/75.0",
        "Accept-Encoding": "gzip, deflate",
        "Accept": "text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8",
        "Accept-Language": "en-US,en;q=0.5",
        "Connection": "keep-alive"
        }
        print(keyword)
        url='https://en.wikipedia.org/w/api.php'
        payload = {'action': 'query', 'titles': keyword,'prop':'pageimages','format':'json','pithumbsize':200}
        r = requests.get(url, params=payload,headers=headers)
        data=json.loads(r.text)
        #get page id
        page_id=list(data['query']['pages'].keys())[0]
         

        #access main page & get thumbnail
        url='https://en.wikipedia.org/?curid='+page_id
        r = requests.get(url,headers=headers)
        html=r.text
        from bs4 import BeautifulSoup
        soup = BeautifulSoup(html, 'lxml')
        thumbnail=soup.find('meta',{'property':'og:image'})['content']
        return thumbnail
    except:
        return 'error'



def wiki_mainpage_url(keyword):
    try:
        headers = {
        "User-Agent": "Mozilla/5.0 (Windows NT 6.1; WOW64; rv:20.0) Gecko/20100101 Firefox/75.0",
        "Accept-Encoding": "gzip, deflate",
        "Accept": "text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8",
        "Accept-Language": "en-US,en;q=0.5",
        "Connection": "keep-alive"
        }
        print(keyword)
        url='https://en.wikipedia.org/w/api.php'
        payload = {'action': 'query', 'titles': keyword,'prop':'pageimages','format':'json','pithumbsize':200}
        r = requests.get(url, params=payload,headers=headers)
        data=json.loads(r.text)
        #get page id
        page_id=list(data['query']['pages'].keys())[0]
         

        #access main page & get thumbnail
        url='https://en.wikipedia.org/?curid='+page_id
          
        return url
    except:
        return 'error'


