import pygsheets
import os
import pandas as pd
from datetime import date
import re
basepath = os.path.dirname(__file__)
keypairs = os.path.abspath(os.path.join(basepath, "jerryparkdfp-6d20c5089776.json"))
gc = pygsheets.authorize(service_file=keypairs)
#gc = pygsheets.authorize(service_file='./git-stacks-a335ce56010a.json')
sh = gc.open_by_key('1sF-4GzQoZ3mMilkWUANzucN5mh4Vpt0Gw3hDSlpvG_k')
text=str(sh.worksheet)
total_sheet_count=int(re.findall('Sheets:(\d+)',text)[0])
print(total_sheet_count)


class Gsheets():
	 
	def __init__(self):
		self.tab_index=None

	def read(self):

		print('start read g doc')
		for i in range(0,total_sheet_count):
			print('Reading '+str(i) +' Doc')
			wks = sh.worksheet('index', i)
			print(wks.title)
			print('\n')
			status_code=str(wks.cell('A2').value)
			if  status_code == '0':
				 
				self.tab_index=i


				#df=pd.DataFrame(wks,columns=1)
				
				df=wks.get_as_df()
				df.name=wks.title
				return df


	def update(self,status_code=None):
			
		print(self.tab_index)
		wks = sh.worksheet('index', self.tab_index)	
		wks.update_value('A2', status_code)

	def get_tabs_status2(self):
		from datetime import date

		today = date.today()

		 

		date = today.strftime("%Y/%m/%d")
		 
		 
		result=[]

		for i in range(0,total_sheet_count):
			print('Reading '+str(i) +' Doc')
			wks = sh.worksheet('index', i)
			status_code=str(wks.cell('A2').value)
			 
			 
			self.tab_index=i
			
			tab_name=wks.title
			tab_link=wks.url
			print(tab_name)
			print(tab_link)
			print('\n\n\n')	

			

			try:
				df=wks.get_as_df()
			except:
				pass

			

			try:		
				
				cell_title=df['title'][0]
			
			except:
				print('---- ERROR-----')
				cell_title=''
			


			try:
				status=df['status'][0]
			except:
				status=''

			try:
				work_date=df['work_date'][0]
			except:
				work_date=''

			try:
				dataset_date=df['dataset_date'][0]
			except:
				dataset_date=''

			result.append((tab_name,tab_link,cell_title,status,work_date,dataset_date))

		
		self.data=result

	def update_history_tab(self):
		#check if there is new update
		history_tab=sh.worksheet('id','1729895840')
		df=history_tab.get_as_df()
		#print(df)

		
		if df['total_sheet_count'][0]==total_sheet_count:
			print('There is No Update')
			exit()

		'''
		for title,tab_link in data:
			print(title,tab_link)
			df['title'] = title
			df['tab_link'] = tab_link
		'''
		 
		##read the g doc and update
		self.get_tabs_status2()
		data=self.data
	
		df=pd.DataFrame({"tab_name":[x[0] for x in data],"tab_link":[x[1] for x in data],"cell_title":[x[2] for x in data],"status":[x[3] for x in data],"work_date":[x[4] for x in data],"dataset_date":[x[5] for x in data]}) 
		df["total_sheet_count"]=total_sheet_count
		df=df.sort_values(by=['work_date'])
		history_tab.set_dataframe(df,'A1')


 

if __name__ == '__main__':
	sheet=Gsheets()
	 
	sheet.update_history_tab()
	#df=sheet.read()
	#print(df)