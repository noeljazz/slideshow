from PIL import Image, ImageDraw, ImageFont,ImageFilter,features
from IPython.display import display
import requests
from io import BytesIO
from _get_df import *
import pandas as pd
import textwrap 
import traceback
from time import sleep
# create Image object with the input image
 

    
class card():
    
    def __init__(self,rank=None,text2=None,text1=None,value=None,poster_url=None):
        self.rank=str(rank)
        self.text2=text2
        self.text1=text1
        self.value=str(value)
        for i in range(3):
            try:
                response = requests.get(poster_url)
                self.poster=Image.open(BytesIO(response.content))
                break
            except:
                try:
                    self.poster=Image.open(poster_url)
                    break
                except:
                    sleep(1)
                    pass
        
    

    def make_value_card(self,indicator):
        card_templete = Image.open('clip/hexagon.png')
        #card_templete = Image.open('clip/medal.png')
        background = ImageDraw.Draw(card_templete)

        ## add rank
        
        value_length=len(self.value)
        
        color = 'rgb(255, 255, 255)' # black color

        font = ImageFont.truetype('arial.ttf', size=24)
        (x, y) = (3, 38)
        
         
        font = ImageFont.truetype('arial.ttf', size=60)    
        text=self.value#' '+'\n '.join(self.value.split(' ')).text2()
        text=text.replace('\n','').strip()
        print('value Length: '+  str(len(text)))


        if value_length>=9:
           font = ImageFont.truetype('arialbd.ttf', size=35)
           x,y=(x+10, y+65)
           

        elif value_length==8:
            print('Value Filter : 0')
            font = ImageFont.truetype('arialbd.ttf', size=42)  
            background.text((x+35, y+55), text, fill=color, font=font,align='left')

        elif value_length ==6 :
            print('Value Filter : 1')
            font = ImageFont.truetype('arialbd.ttf', size=55)
            background.text((x+30, y+50), text, fill=color, font=font,align='left')
        elif value_length == 7:
            font = ImageFont.truetype('arialbd.ttf', size=55)
            background.text((x+20, y+55), text, fill=color, font=font,align='left')
            print('Value Filter : 2')
        elif value_length ==5:
            font = ImageFont.truetype('arialbd.ttf', size=55)
            background.text((x+40, y+55), text, fill=color, font=font,align='left')
            print('Value Filter : 3')
        elif value_length == 4:
            font = ImageFont.truetype('arialbd.ttf', size=55)
            background.text((x+65, y+55), text, fill=color, font=font,align='left')
            print('Value Filter : 4')

        elif value_length ==3:
            font = ImageFont.truetype('arialbd.ttf', size=55)
            background.text((x+65, y+55), text, fill=color, font=font,align='left')
            print('Value Filter : 5')

        elif value_length ==2:
            font = ImageFont.truetype('arialbd.ttf', size=55)
            background.text((x+80, y+55), text, fill=color, font=font,align='left')
            print('Value Filter : 5')

        elif value_length ==1:
            font = ImageFont.truetype('arialbd.ttf', size=55)
            background.text((x+97, y+55), text, fill=color, font=font,align='left')
            print('Value Filter : 5')


        font = ImageFont.truetype('arialbd.ttf', size=20)  
        i_len=len(indicator)
        print('Indicator Length : '+str(i_len))
        
        y=y+150

        if i_len==15:
            font = ImageFont.truetype('arialbd.ttf', size=18)  
            x=x+55
        elif i_len==12:
            x=x+65
            y=y+150
        elif i_len==11:
            x=x+65
            y=y+150
        elif i_len==10:
            x=x+54
            y=y+150 
        elif i_len==9:
            x=x+80
            y=y+150
        elif i_len==6:
            x=x+90
            y=y+150

        elif i_len==5:
            x=x+93
            y=y+150


        background.text((x, y), indicator, fill=color, font=font,align='left')
            #rank = "100"
        
        
        card_templete.save('img_value/{:03d}.png'.format(int(self.rank)))
        

    def make_card(self):
        ###select card templete
        card_templete = Image.open('clip/1.png')
        background = ImageDraw.Draw(card_templete)

        # create font object with the font file and specify
        # desired size

        ## add rank
        font = ImageFont.truetype('arialbd.ttf', size=28)
        if len(self.rank) ==3: 
            font = ImageFont.truetype('arialbd.ttf', size=24)
            (x, y) = (136, 38)
        elif len(self.rank)==2:
            (x, y) = (141, 38)
        elif len(self.rank)==1:
            (x, y) = (148, 36)
        

                #rank = "100"
        color = 'rgb(255, 255, 255)' # black color

        background.text((x, y), self.rank, fill=color, font=font,align='left')



        ###Add text2

        size=45
        wrapper = textwrap.TextWrapper(width=20) 
        text2_length=len(self.text2)
        print('text2 length:' + str(text2_length))
        
        (x, y) = (20, 530)

        if text2_length>=40:
            print('text2 Length filter: 1')
            size=size-20 
            wrapper = textwrap.TextWrapper(width=25) 

        elif text2_length > 30 and text2_length < 40:
            print('text2 Length filter: 2')
            wrapper = textwrap.TextWrapper(width=20) 
            size=size-18

        elif text2_length > 20 and text2_length <= 30:
            print('text2 Length filter: 3')
            wrapper = textwrap.TextWrapper(width=18) 
            size=size-13

       
        elif text2_length > 10 and text2_length <= 20 :
            print('text2 Length filter: 4')
            wrapper = textwrap.TextWrapper(width=15) 
            size=size-10

        elif text2_length <= 10:
            print('text2 Length filter: 5')
            wrapper = textwrap.TextWrapper(width=15) 
            size=size

 

 
        print('text2 size: '+ str(size))
        text=self.text2
        font = ImageFont.truetype('arial.ttf', size=size)
        
        color = 'rgb(255, 255, 255)' # white color
        
        word_list = wrapper.wrap(text=self.text2) 
        text='\n'.join(word_list)

        background.multiline_text((x, y), text, fill=color, font=font,align='left')
        ##############


        ### Add text1

        (x, y) = (75, 475)
        size=33
        text1_length=len(self.text1)
        print('text1_length : '+str(text1_length))
        if text1_length == 11:
            print('text1_length filter : 1')
            x=x-15
        elif text1_length>11 and text1_length <15:
            print('text1_length filter : 2')
            x=x-40
        elif text1_length <8 and text1_length>4 :
           print('text1_length filter : 3')
           x=x+15
        elif text1_length>=15 and text1_length<=18:
            print('text1_length filter : 4')
            x=x-50
            size=size-6
        elif text1_length > 18  and text1_length <= 22:
            print('text1_length filter : 5')
            x=x-55
            size=size-7

        elif text1_length>22:
            print('text1_length filter : 6')
            wrapper = textwrap.TextWrapper(width=15) 
            word_list = wrapper.wrap(text=self.text1) 
            text='\n'.join(word_list)
            self.text1=text
            x=x-50
            y=y-10
            size=size-10

        elif text1_length==4:  ## Year Value
            print('text1_length filter : 7')
            x=x+20
            y=y-10
            size=size+18

        print('size : '+str(size))   
        font = ImageFont.truetype('arialbd.ttf', size=size)
        color = 'rgb(255, 255, 255)' # white color
        background.text((x, y), self.text1, fill=color, font=font,align='left')

 

        ##save file 
        card_templete.save('img_cards/{:03d}.png'.format(int(self.rank)))

       



        ##Overlay Image
        background=Image.open('img_cards/{:03d}.png'.format(int(self.rank))).convert("RGBA")
        w,h=background.size  #309,621
        foreground = self.poster
        newsize = (293, 371) #default size
        #newsize = (293, 271) #Flag size
        foreground=foreground.resize(newsize, Image.ANTIALIAS) 

        foreground_mask = foreground.convert("RGBA")
        
        background.paste(foreground,(10,92),foreground_mask)
        background.save('img_cards/{:03d}.png'.format(int(self.rank)))


        #background.paste(background, (0, 0), foreground)
        #display(background)

#df=pd.read_csv('Fifa Ranking 2020.csv')

def value_card_only():
    for i in range(1,100+1):  
        generate_card=card(rank=i,text2='',value=i,poster_url='')
        #generate_card.make_card()
        generate_card.make_value_card()
from time import sleep
##value_card_only()
#exit()

df=pd.read_csv('db_parsed/NTTT- Source - Historical Disney Movies.csv',skiprows=0)


def single_test(i):
    i=i-1

    try:

        rank=df.iloc[i]['rank']
        #year=df.iloc[i].get('year')
        value=str(df.iloc[i]['value']).strip()
        text1=str(df.iloc[i]['name']).strip()
        text2=str(df.iloc[i]['year']).strip()
        poster_url=df.iloc[i]['image_url']
        
        
        print('\n')
        print(rank,text2)
        indicator='Score'
        generate_card=card(rank=rank,text2=text1,text1=text2,value=value,poster_url=poster_url)
        generate_card.make_card()
        generate_card.make_value_card(indicator)
    except :
        print (traceback.format_exc())

 
#single_test(173)
#exit()

def run():
    
    #print(df.shape)
    

    for i in range(df.shape[0]):
        
        try:
        
            rank=df.iloc[i]['rank']
            #year=df.iloc[i].get('year')
            value=''#str(df.iloc[i]['value']).strip()
            text1=str(df.iloc[i]['name']).strip() 
            text2=str(df.iloc[i]['date']).strip()
            poster_url=df.iloc[i]['image_url']
            
            print('\n')
            print(rank,text2)
            indicator='Number of Votes'
            generate_card=card(rank=rank,text2=text1,text1=text2,value=value,poster_url=poster_url)
            generate_card.make_card()
            #generate_card.make_value_card(indicator)
        except :
            print (traceback.format_exc())

        

run()
#single_test(rank=90)
#from time import sleep
#for i in range(40,101):
#    single_test(rank=i)
#    sleep(1)
#